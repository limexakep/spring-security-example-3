package com.example.springsecurityexample.repository;

import java.util.*;

import com.example.springsecurityexample.entity.*;
import org.springframework.data.repository.*;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
