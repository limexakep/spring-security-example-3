package com.example.springsecurityexample.controller;

import javax.servlet.http.*;

import org.springframework.http.*;
import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class HelloController {
    @GetMapping("/all")
    public ResponseEntity<String> sayHelloAll() {
        return ResponseEntity.ok("Hello all");
    }
    @PreAuthorize("hasAuthority('ROLE_USER')")
    @GetMapping("/user")
    public ResponseEntity<String> sayHelloUser(HttpServletRequest request) {
        return ResponseEntity.ok("Hello " + request.getUserPrincipal().getName());
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @GetMapping("/admin")
    public ResponseEntity<String> sayHelloAdmin(HttpServletRequest request) {
        return ResponseEntity.ok("Hello Admin " + request.getUserPrincipal().getName());
    }
}
