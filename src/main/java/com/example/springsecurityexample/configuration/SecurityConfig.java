package com.example.springsecurityexample.configuration;

import javax.sql.*;

import com.example.springsecurityexample.service.*;
import lombok.*;
import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.*;
import org.springframework.security.config.annotation.method.configuration.*;
import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.*;
import org.springframework.security.crypto.password.*;
import org.springframework.security.web.*;

@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;

    private final UserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/api/all").permitAll()
            .antMatchers("/api/user").hasAuthority("ROLE_USER")
            .antMatchers("/api/admin").hasAuthority("ROLE_ADMIN")
            .and()
            .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /*auth
            .inMemoryAuthentication()
            .passwordEncoder(passwordEncoder())
            .withUser("user1")
            .password(passwordEncoder().encode("user1"))
            .authorities("ROLE_USER")
            .and()
            .withUser("user2")
            .password(passwordEncoder().encode("user2"))
            .authorities("ROLE_ADMIN");*/
        /*auth
            .jdbcAuthentication()
            .dataSource(dataSource)
            .usersByUsernameQuery("Select username, password, enabled from \"user\" where username = ?")
            .authoritiesByUsernameQuery(
                "SELECT \"user\".username as username, role.name as role FROM \"user\" " +
                "INNER JOIN user_role ON \"user\".id = user_role.user_id " +
                "INNER JOIN role ON user_role.role_id = role.id WHERE \"user\".username = ?");*/

        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}